## Run tests

You can run the testsuite via

```
nix-build tests/intern.nix
nix-build tests/extern.nix
```

## Nixops

You can test the setup via `nixops`. After installation, do

```
nixops create nixops/single-server.nix nixops/vbox.nix -d mail
nixops deploy -d mail
nixops info -d mail
```

You can then test the server via e.g. `telnet`. To log into it, use

```
nixops ssh -d mail mailserver
```

## Imap
To test imap manually use

```
openssl s_client -host mail.example.com -port 143 -starttls imap
```
